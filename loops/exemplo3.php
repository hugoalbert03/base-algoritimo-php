<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
		<link rel="icon" href="https://pngimg.com/uploads/php/php_PNG29.png">
		<title>Loops::Exemplo 03</title>
	</head>
	<body>
	<h1>Exemplo 03</h1>
	<a href="index.php">Voltar</a>
		<?php 
		  $nm = 1; # Atribuiu a variável
		  for(;;){ #inicia-se o laço de repetição sem nenhum parâmetro definido
		      if($nm > 1000){ #Acondição força a parada do loop em um determinado limite
		          break;
		      }
		      echo "$nm "; #Escreve-se o valor
		      $nm++; #concartena o valor
		  }
		?>
	</body>
</html>