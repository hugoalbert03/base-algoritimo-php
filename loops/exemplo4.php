<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
		<link rel="icon" href="https://pngimg.com/uploads/php/php_PNG29.png">
		<title>Loops::Exemplo 04</title>
	</head>
	<body>
	<h1>Exemplo 04</h1>
	<a href="index.php">Voltar</a>
		<?php 
		  for($numb = 1, $vr = 0; $numb <= 100; $vr += $numb, print "$numb ", $numb++);
		?>
	</body>
</html>