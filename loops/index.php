<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
		<link rel="icon" href="https://pngimg.com/uploads/php/php_PNG29.png">
		<title>Loops</title>
	</head>
	<body>
		<h1>Loops</h1>
		<hr>
		<ul>
			<li><a href="exemplo1.php">Exemplo 1</a>
			<li><a href="exemplo2.php">Exemplo 2</a>
			<li><a href="exemplo3.php">Exemplo 3</a>
			<li><a href="exemplo4.php">Exemplo 4</a>
		</ul>
	</body>
</html>