<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
		<link rel="icon" href="https://pngimg.com/uploads/php/php_PNG29.png">
		<title>Arrays</title>
	</head>
	<body>
		<?php 
		#Array com chave
		      #Modelo 1
		      $teste = array("Anel","Caixa","Ônibus","Moto");
		      echo $teste[1];
		      var_dump($teste);
		      
		      #Modelo 2
		      $new_teste = ["Baú","Escada","Casa","Rua"];
		      echo $new_teste[2];
		      var_dump($new_teste);
		#Array como nome do objeto
		      #Modelo 1
	          $teste_array = [
	              "nome1" => "Hugo",
	              "nome2" => "Enzo",
	              "nome3" => "Noemi"
	          ];
	          
	          echo $teste_array['nome3'];
	          var_dump($teste_array);
	          
	          #Modelo 2
	          $new_teste_array = array(
	              "nm1" => "Iza",
	              "nm2" => "Zulmira",
	              "nm3" => "Joao"
	          );
		      echo $new_teste_array['nm3'];
		      var_dump($new_teste_array);
		?>
	</body>
</html>