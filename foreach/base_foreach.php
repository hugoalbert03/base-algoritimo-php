<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
		<link rel="icon" href="https://pngimg.com/uploads/php/php_PNG29.png">
		<title>Aprendendo Foreach</title>
	</head>
	<body>
		<h1>Aprendendo a usar o Foreach</h1>
		<hr>
		<strong>Exemplo 1</strong>
		<?php 
		  $cidades = ['Varjão','Planaltina','Paranoá','Patk Way'];
		  foreach($cidades as $index => $cidade){
		      echo "<p>A referência é $cidade no Índice $index</p>";
		  };
		  echo "<hr>";
		  echo"<strong>Exemplo 2</strong>";
          $nomes = [
              "vaga1" => 'Maria',
              "vaga2" => 'Paula',
              "vaga3" => 'José',
              "vaga4" => 'Pedro'
              
          ];
		  foreach($nomes as $index => $nome){
		      echo "<p>O nome é $nome no Índice $index</p>";
		  };
		?>
	</body>
</html>