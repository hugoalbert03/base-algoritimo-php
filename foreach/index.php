<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
		<link rel="icon" href="https://pngimg.com/uploads/php/php_PNG29.png">
		<title>Foreach</title>
	</head>
	<body>
		<h1>Foreach</h1>
		<hr>
		<?php 
		  $conj = array(1, 2, 3, 4);
		  foreach ($conj as &$bloco){
		      $bloco = $bloco * 2;
		      echo "$bloco ";
		  }
		  
		  foreach ($conj as $key => $bloco){
		      echo "{$chave} => {$valor}";
		      print_r($conj);
		  }
		  #$unset($valor);
		?>
	</body>
</html>